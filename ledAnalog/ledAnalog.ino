const int analogInPin = A0;
const int analogOutPin= 3;
int valorPotenciometro =0;
int outValor =0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  valorPotenciometro = analogRead(analogInPin);
  outValor = map(valorPotenciometro,0,1023,0,255);
  analogWrite(analogOutPin,outValor);
  Serial.print("Potenciometro=");
  Serial.print(valorPotenciometro);
  Serial.print("\t PWM = ");
  Serial.println(outValor);
  delay(10);
}
