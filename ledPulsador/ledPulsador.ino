const int led = 13;
const int boton = 7;
int val =0;
int state =0;
int old =0;

void setup() {
  pinMode(led,OUTPUT);
  pinMode(boton,INPUT);

}

void loop() {
  val = digitalRead(boton);
  if( (val == HIGH) && (old==LOW)){
    state = 1-state;
    delay(10);
    }  
  old = val;
  if(state==1){
    digitalWrite(led,HIGH);
    }
    else{
      digitalWrite(led,LOW);
      }
}
