#define sensor A0
#define led1 3
#define buzzer 5
#define ledEmergencia 7
int lectura,equivalente,medicion,contador;
void setup() {
  contador=0;
  pinMode(buzzer,OUTPUT);
  pinMode(led1,OUTPUT);
  pinMode(ledEmergencia,OUTPUT);
  Serial.begin(9600);

}

void loop() {
  lectura = valorPotenciometro();
  Serial.println(lectura);
  equivalente= map(lectura,0,1023,0,255);
  analogWrite(led1,equivalente);

  if(lectura<500){
    digitalWrite(buzzer,LOW);
    digitalWrite(ledEmergencia,LOW);
    }
    else if(lectura >=500 && lectura < 750){
      digitalWrite(ledEmergencia,LOW);
      valorBuzzer();
      delay(200);
      }
      else if(lectura >750){
        digitalWrite(ledEmergencia,HIGH);
        digitalWrite(buzzer,HIGH);
        }

}

int valorPotenciometro() {
  medicion = 0;
  for (int i = 0; i < 5; i++) {
    medicion += analogRead(sensor);
  }
  medicion = medicion / 5;
  return medicion;
}

void valorBuzzer() {
  if (contador == 0) {
    digitalWrite(buzzer, !digitalRead(buzzer));
  } 
   contador++;
  if (contador == 5) {
    contador = 0;
  }
}
