#include <Buzzer.h>

#include <Pulsador.h>

 int pinPulsador= 9;
 int pinBuzzer= 3;


Pulsador pulsador(pinPulsador);

Buzzer buzzer(pinBuzzer);

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  if(pulsador.pulsar()==1){
    buzzer.beepTone(440,2000);
    delay(500);
    }

}
