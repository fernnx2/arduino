/* 
   -- New project -- 
    
   This source code of graphical user interface  
   has been generated automatically by RemoteXY editor. 
   To compile this code using RemoteXY library 2.3.5 or later version  
   download by link http://remotexy.com/en/library/ 
   To connect using RemoteXY mobile app by link http://remotexy.com/en/download/                    
     - for ANDROID 4.3.1 or later version; 
     - for iOS 1.3.5 or later version; 
     
   This source code is free software; you can redistribute it and/or 
   modify it under the terms of the GNU Lesser General Public 
   License as published by the Free Software Foundation; either 
   version 2.1 of the License, or (at your option) any later version.     
*/ 

////////////////////////////////////////////// 
//        RemoteXY include library          // 
////////////////////////////////////////////// 

// RemoteXY select connection mode and include library  
#define REMOTEXY_MODE__SOFTSERIAL
#include <SoftwareSerial.h> 

#include <RemoteXY.h> 

// RemoteXY connection settings  
#define REMOTEXY_SERIAL_RX 2 
#define REMOTEXY_SERIAL_TX 3 
#define REMOTEXY_SERIAL_SPEED 9600 


// RemoteXY configurate   
#pragma pack(push, 1) 
uint8_t RemoteXY_CONF[] = 
  { 255,1,0,0,0,116,0,8,29,0,
  4,128,31,33,35,9,133,8,129,0,
  2,3,96,5,1,83,105,115,116,101,
  109,97,32,100,101,32,83,101,103,117,
  114,105,100,97,100,32,67,111,110,116,
  114,97,32,73,110,99,101,110,100,105,
  111,115,0,129,0,7,33,18,5,6,
  77,195,173,110,105,109,111,0,129,0,
  73,33,19,5,1,77,195,161,120,105,
  109,111,0,129,0,18,16,63,6,133,
  65,106,117,115,116,101,32,100,101,32,
  83,101,110,115,105,98,105,108,105,100,
  97,100,0 }; 
   
// this structure defines all the variables of your control interface  
struct { 

    // input variable
  int8_t calibrar; // =0..100 slider position 

    // other variable
  uint8_t connect_flag;  // =1 if wire connected, else =0 

} RemoteXY; 
#pragma pack(pop) 

///////////////////////////////////////////// 
//           END RemoteXY include          // 
///////////////////////////////////////////// 



void setup()  
{ 
  RemoteXY_Init ();  
   
   
  // TODO you setup code 
   
} 

void loop()  
{  
  RemoteXY_Handler (); 
   
   
  // TODO you loop code 
  // use the RemoteXY structure for data transfer 


}
