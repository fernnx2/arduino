
const byte led = D4;
const byte potenciometro = A0;
const byte pulsador =D0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(led,OUTPUT);
  pinMode(potenciometro,OUTPUT);
  pinMode(pulsador,INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(pulsador)== HIGH){
    digitalWrite(led,HIGH);
    }
    int valor =analogRead(potenciometro);
    Serial.println(valor);
}
