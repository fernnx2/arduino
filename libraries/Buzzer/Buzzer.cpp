/*
* @Copyright
* Libreria para controlar el sonido del buzzer
* autor <fernnx2 devfernando95@gmail.com>
* Licencia GPL
*/

#include "Arduino.h"
#include "Buzzer.h"

Buzzer::Buzzer(byte pin)
{
    _pin = pin;
    pinMode(_pin, OUTPUT);
}

int Buzzer::beep(int pausa)
{
    digitalWrite(_pin, 50);
    delay(pausa);
    digitalWrite(_pin, 0);
    delay(pausa);
}

int Buzzer::beepTone(int frecuencia, int duration)
{
    tone(_pin, frecuencia, duration);
    delay(duration);
}
