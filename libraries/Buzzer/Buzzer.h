/*
* @Copyright
* Libreria para controlar el sonido del buzzer
* autor <fernnx2 devfernando95@gmail.com>
* Licencia GPL
*/

#ifndef Buzzer_h
#define Buzzer_h
#include "Arduino.h"

class Buzzer
{
  public:
    Buzzer(byte pin);
    int beep(int pausa);
    int beepTone(int frecuencia, int duration);

  private:
    byte _pin;
};

#endif