/*
Clase encargada de obtener las pulsaciones
 @copyright
 fernnx2 <devfernando95@gmail.com>
 Licencia GPL
*/

#ifndef Pulsador_h
#define Pulsador_h
#include "Arduino.h"

class Pulsador
{
public:
  
  Pulsador(byte pin);
  void setPin(byte pin);
  byte getPin();
  int pulsar();
  void setPulsaciones(int pulsaciones);
  int getPulsaciones();
  void resetPulsaciones();
  void aumentarPulsaciones(int pulsaciones);

private:
  byte _pin;
  int _pulsaciones;
};
#endif
