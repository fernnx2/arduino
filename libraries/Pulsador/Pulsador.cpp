#include "Arduino.h"
#include "Pulsador.h"

Pulsador::Pulsador(byte pin)
{
    _pin = pin;
 
}
int Pulsador::pulsar()
{
    if (digitalRead(_pin) == HIGH)
    {
        _pulsaciones = _pulsaciones + 1;
        delay(500);
        return 1;
    }
    else
    {
        return 0;
    }
}

void Pulsador::setPin(byte pin){
	_pin = pin;
}

byte Pulsador::getPin(){
	return _pin;
}


void Pulsador::setPulsaciones(int pulsaciones)
{
    _pulsaciones = pulsaciones;
}
int Pulsador::getPulsaciones()
{
    return _pulsaciones;
}
void Pulsador::resetPulsaciones()
{
    _pulsaciones = 0;
}
void Pulsador::aumentarPulsaciones(int pulsaciones)
{
    _pulsaciones = _pulsaciones + pulsaciones;
}
