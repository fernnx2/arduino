/*
* @Copyright
* Libreria para controlar el sonido del buzzer
* autor <fernnx2 devfernando95@gmail.com>
* Licencia GPL
*/

#include "Arduino.h"
#include "Sensor.h"

Sensor::Sensor(byte pinAnalogo){
        _pinAnalogo = pinAnalogo;
        _analogCheck = 0;
}

int Sensor::valorAnalogo(){
    return analogRead(_pinAnalogo);
}

int Sensor::valorDecimal(int valorAnalogo,int calibracion){
    int valor =map(valorAnalogo, 0, 1023, 0, 255);
    return (valor/calibrar(calibracion));
}

int Sensor::getAnalogCheck(){
    return _analogCheck;
}

void Sensor::setAnalogCheck(int analogCheck){
    _analogCheck = analogCheck;
}

double Sensor::calibrar(int calibracion){
    if(calibracion >= -100 && calibracion <= -80){
        return 25.5;
    }
    else if(calibracion >=-79 && calibracion <=60){
        return 24.5;
    }
    else if(calibracion >=59 && calibracion <=40){
        return 23.5;
    }
    else if(calibracion >=39 && calibracion <=20){
        return 22.5;
    }
    else if(calibracion >=19 && calibracion <=1){
        return 21.5;
    }
    else if(calibracion == 0){
        return 20.5;
    }
    else if(calibracion >=1 && calibracion <=20){
        return 19.5;
    }
    else if(calibracion >=21 && calibracion <=40){
        return 18.5;
    }
    else if(calibracion >=41 && calibracion <=60){
        return 17.5;
    }
    else if(calibracion >=61 && calibracion <=80){
        return 16.5;
    }
    else {
        return 15.5;
    }
}
