/*
* @Copyright
* Libreria para controlar el sonido del buzzer
* autor <fernnx2 devfernando95@gmail.com>
* Licencia GPL
*/

#ifndef Sensor_h
#define Sensor_h
#include "Arduino.h"

class Sensor
{
    public:
    Sensor(byte pinAnalogo);

    int valorAnalogo();
    int valorDecimal(int valorAnalogo,int calibracion);
    int getAnalogCheck();
    void setAnalogCheck(int analogCheck);
    double calibrar(int calibracion);

    private:
    byte _pinAnalogo;
    byte _pinDigital;
    int _analogCheck;
};

#endif