#include "Arduino.h"
#include "Display.h"

//Display::Display() {}
Display::Display(byte array[])
{
    byte cero[] = {1, 1, 1, 1, 1, 0, 1};
    byte uno[] = {0, 1, 1, 0, 0, 0, 0, 0};
    byte dos[] = {1, 1, 0, 1, 1, 1, 0};
    byte tres[] = {1, 1, 1, 1, 0, 1, 0};
    byte cuatro[] = {0, 1, 1, 0, 0, 1, 1};
    byte cinco[] = {1, 0, 1, 1, 0, 1, 1};
    byte seis[] = {1, 0, 1, 1, 1, 1, 1};
    byte siete[] = {1, 1, 1, 0, 0, 0, 0};
    byte ocho[] = {1, 1, 1, 1, 1, 1, 1};
    byte nueve[] = {1, 1, 1, 1, 0, 1, 1};

    for (int x = 0; x < 7; x++)
    {
        _pinesDisplay[x] = array[x];
        pinMode(_pinesDisplay[x], OUTPUT);
        _cero[x] = cero[x];
        _uno[x] = uno[x];
        _dos[x] = dos[x];
        _tres[x] = tres[x];
        _cuatro[x] = cuatro[x];
        _cinco[x] = cinco[x];
        _seis[x] = seis[x];
        _siete[x] = siete[x];
        _ocho[x] = ocho[x];
        _nueve[x] = nueve[x];
    }
}

void Display::encenderDisplay(byte numero)
{
    switch (numero)
    {
    case 0:
        encender(_cero);
        break;
    case 1:
        encender(_uno);
        break;
    case 2:
        encender(_dos);
        break;
    case 3:
        encender(_tres);
        break;
    case 4:
        encender(_cuatro);
        break;
    case 5:
        encender(_cinco);
        break;
    case 6:
        encender(_seis);
        break;
    case 7:
        encender(_siete);
        break;
    case 8:
        encender(_ocho);
        break;
    case 9:
        encender(_nueve);
        break;

    default:
        encender(_cero);
        break;
    }
}

void Display::apagarDisplay()
{
    for (int y = 0; y < 7; y++)
    {
        digitalWrite(_pinesDisplay[y], LOW);
    }
}

void Display::encender(byte arrayNumero[])
{
    for (int y = 0; y < 7; y++)
    {
        if (arrayNumero[y] == 0)
        {
            digitalWrite(_pinesDisplay[y], LOW);
        }
        else
        {
            digitalWrite(_pinesDisplay[y], HIGH);
        }
    }
}
