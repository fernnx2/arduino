/*
Clase creada para la manipulacion de un display
 @copyright
 fernnx2 <devfernando95@gmail.com>
 Licencia GLP
*/

#ifndef Display_h
#define Display_h
#include "Arduino.h"

class Display
{
  public:
	Display(byte pines[]);
	void encenderDisplay(byte numero);
	void apagarDisplay();
	void encender(byte arrayNumero[]);

  private:
	byte _cero[7];
	byte _uno[7];
	byte _dos[7];
	byte _tres[7];
	byte _cuatro[7];
	byte _cinco[7];
	byte _seis[7];
	byte _siete[7];
	byte _ocho[7];
	byte _nueve[7];
	byte _pinesDisplay[8];
};
#endif
