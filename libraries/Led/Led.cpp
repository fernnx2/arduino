/*
 Copyright  
 fernnx2 <devfernando95@gmail.com>
*/

#include "Arduino.h"
#include "Led.h"

Led::Led(byte pin)
{
    _pin = pin;
    pinMode(_pin,OUTPUT);
}

int Led::on()
{

    digitalWrite(_pin, HIGH);
    _state = 1;
    return 1;
}

int Led::off()
{

    digitalWrite(_pin, LOW);
    _state = 0;
    return 0;
}

byte Led::getState()
{
    return _state;
}

void Led::setPin(byte pin)
{
    _pin = pin;
}

byte Led::getPin()
{
    return _pin;
}
