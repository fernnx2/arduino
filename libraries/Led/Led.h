/*
 @copyright
 fernnx2 <devfernando95@gmail.com>
 Licencia GLP
*/

#ifndef Led_h
#define Led_h
#include "Arduino.h"

class Led
{
  public:
    Led(byte pin);
    on();

    off();

    byte getState();
    void setPin(byte pin);
    byte getPin();

  private:
    byte _pin;
    byte _state;
};

#endif