#include <Pulsador.h>

#include <Led.h>
#define pinLed1 2
#define pinLed2 3
#define pinLed3 4
#define pinPulsador 5

Led Led1(pinLed1);
Led Led2(pinLed2);
Led Led3(pinLed3);
Pulsador Pulsador(pinPulsador);

void setup() {
  Serial.begin(9600);
  pinMode(Led1.getPin(),OUTPUT);
  pinMode(Led2.getPin(),OUTPUT);
  pinMode(Led3.getPin(),OUTPUT);
  pinMode(Pulsador.getPin(),INPUT);

}

void loop() {

  if(Pulsador.pulsar()==1 ){
   Serial.println(Pulsador.getPulsaciones()); 

   
    delay(500);
    Led1.on();
    delay(500);
    Led2.on();
    delay(500);
    Led3.on();
     delay(500);
    Led1.off();
    delay(500);
    Led2.off();
    delay(500);
    Led3.off();
   
   }
   
    

    
    

}
