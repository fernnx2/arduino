#include <Sensor.h>
byte pinSensorA = A0;
byte pinSensorD = 2;
Sensor sensor(pinSensorA,pinSensorD);
void setup() {
  Serial.begin(9600);
  Serial.println("Sensor Activado --Obteniendo Medicion de Gas--");
  attachInterrupt(digitalPinToInterrupt(pinSensorD),deteccion,CHANGE);

}

int analogCheck = sensor.getAnalogCheck();

void loop() {
  if(sensor.getAnalogCheck() == 1){
    if(sensor.valorDecimal() >= 5 && sensor.valorDecimal() <=8){
      Serial.println("Warning!!! Detectando valor de peligro..." + sensor.valorDecimal());
      }
      else if(sensor.valorDecimal() >8 && sensor.valorDecimal() <=10){
        Serial.println("Detectanto Valor Critico ..." + sensor.valorDecimal());
        Serial.println("Accionando dispositivo de Seguridad en 3 Segundos!!! Evacue la Zona...");
        delay(3000);
        Serial.println("Sistema de gas apagado satisfactoriamente");
        }
        else{
          Serial.println("Presencia de Gas en valores Normales..." + + sensor.valorDecimal());
          }
      sensor.setAnalogCheck(0);
    }

}

void deteccion(){
    // Leemos el valor del pin digital 
  int digitalValue = digitalRead(pinSensorD);
  
  // Si es 1, significa que no hay gas detectado
  if (digitalValue && analogCheck) {
    Serial.println("Presencia de gas en valores normales...");
    analogCheck = 0;
  }
  // Si es 0, detectamos gas. Comenzamos lecturas analógicas...
  if (!digitalValue && !analogCheck) {
    Serial.println("Gas detectado, comenzando a leer valores...");
    analogCheck = 1;
  }
}
