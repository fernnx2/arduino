/* 
   Sistema de Seguridad Anti-incendios.   
*/ 



// RemoteXY select connection mode and include library  
#define REMOTEXY_MODE__SOFTSERIAL
#include <SoftwareSerial.h> 

#include <RemoteXY.h> 

// RemoteXY connection settings  
#define REMOTEXY_SERIAL_RX 2 
#define REMOTEXY_SERIAL_TX 3 
#define REMOTEXY_SERIAL_SPEED 9600 

#include <Servo.h>
#include <Sensor.h>
#include <Pulsador.h>
#include <Display.h>
#include <Buzzer.h>
#include <GFButton.h>

byte pinesDisplay[] ={4,5,6,7,8,9,10};
const byte pinBuzzer = 11;
const byte pinServo = 12;
const byte pinSensor = A0;
const byte pinPulsador =13;
unsigned long previousMillis =0;
const long interval = 500;

GFButton pulsador(pinPulsador);
Buzzer buzzer(pinBuzzer);
Display display(pinesDisplay);
Sensor sensor(pinSensor);
Servo servo;
bool estado =false;
bool estado_anterior = false;


// RemoteXY configurate   
#pragma pack(push, 1) 
uint8_t RemoteXY_CONF[] = 
  { 255,1,0,0,0,125,0,8,16,0,
  4,160,30,35,39,13,2,26,129,0,
  6,7,90,5,1,83,105,115,116,101,
  109,97,32,100,101,32,83,101,103,117,
  114,105,100,97,100,32,65,110,116,105,
  45,73,110,99,101,110,100,105,111,115,
  0,129,0,19,20,64,4,12,65,106,
  117,115,116,101,32,100,101,32,83,101,
  110,115,105,98,105,108,105,100,97,100,
  32,100,101,108,32,83,101,110,115,111,
  114,0,129,0,5,36,18,6,6,77,
  195,173,110,105,109,111,0,129,0,69,
  36,22,6,36,77,195,161,120,105,109,
  111,0 }; 
   
// this structure defines all the variables of your control interface  
struct { 

    // input variable
  int8_t calibrar; // =0..100 slider position 

    // other variable
  uint8_t connect_flag;  // =1 if wire connected, else =0 

} RemoteXY; 
#pragma pack(pop) 

///////////////////////////////////////////// 
//           END RemoteXY include          // 
///////////////////////////////////////////// 



void setup()  
{ 
  RemoteXY_Init ();  
  Serial.begin(9600);
  //Serial.println(F("-------Iniciando Sistema de Protección Anti-Encendios---------"));
  servo.attach(pinServo);
  buzzer.beepTone(2048,400);
  servo.write(0);
   
} 

void loop()  
{  
  RemoteXY_Handler (); 
  int calibracion = RemoteXY.calibrar; 
 
    int valor = sensor.valorAnalogo();
    int valorDecimal = sensor.valorDecimal(valor,calibracion);
    if(valorDecimal>=0 && valorDecimal<=5){
      //Serial.println("Valores  de Temperatura y Humo en parametros Normales");
      display.encenderDisplay(valorDecimal);
      }
    if(valorDecimal>=6 && valorDecimal<=7){
      //Serial.println("Valores  de Temperatura y Humo WARNING!!!!....");
      buzzer.beepTone(2048,500);
      delay(500);
      display.encenderDisplay(valorDecimal);
      }
      
    if(valorDecimal>7){
      //Serial.println("Valores  de Temperatura y Humo críticos DANGER!!!!....");
      //Serial.println("Accionando dispositivo Anti-Encendios!");
      display.encenderDisplay(valorDecimal);
      for(int x =0; x<7;x++){
        buzzer.beepTone(2048,100);
        }
      
       delay(2000);
       servo.write(0);
       delay(500);
       servo.write(180);
       delay(1000);
       servo.write(0);
       delay(1000);
      }  
    Serial.print("Valor Calibracion:  ");
    Serial.println(calibracion);
    delay(500);
    if(estado){
          test();
          
    }
    unsigned long currentMillis = millis();

    if(currentMillis  - previousMillis >= interval){
      previousMillis = currentMillis;
      estado = pulsador.isPressed();
        
      } 


}

void test(){
    //Serial.println("---Test---");
    buzzer.beepTone(2048,700);
    delay(1000);
    display.encenderDisplay(9);
    delay(1000);
    servo.write(180);
    delay(1000);
    servo.write(0);
    delay(1000);
  
  }
