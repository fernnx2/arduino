#include <Servo.h>

Servo servomotor;
void setup() {
  //Iniciamos el monitor de Serie
  Serial.begin(9600);
  //Iniciamos el servo para que empieze a trabajar con el pin 9
  servomotor.attach(9);

}

void loop() {
  // Desplazamos el servo a la posicion cero 0
  Serial.println("girando a 0 grados");
  servomotor.write(0);
  delay(1000);
  Serial.println("girando a 180 grados");
  servomotor.write(180);
  delay(1000);

}
